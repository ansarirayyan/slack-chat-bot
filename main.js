const { App } = require('@slack/bolt');
const config = require('config');
const fs = require('fs');

const app = new App({
  signingSecret: config.get("SLACK_SIGNING_SECRET"),
  token: config.get("SLACK_BOT_TOKEN"),
});

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}

app.event('app_mention', ({ event, say }) => {
    say(`Hello world, <@${event.user}>!`);
});

(async () => {
  // Start the app
  await app.start(3000);

  console.log('⚡️ Bolt app is running!');
})();

app.command('/sync', async ({ command, ack, client, say }) => {
  // Acknowledge command request
  await ack("Saving the chat...");

console.log(command);
// console.log(":::::THE CHANNEL ID IS: " + command.channe)

  // Store conversation history
  let conversationHistory;
  // ID of channel you watch to fetch the history for
  let channelId = command.channel_id;

  try {
    // Call the conversations.history method using WebClient
    const result = await client.conversations.history({
      channel: channelId
    });

    conversationHistory = result.messages;
console.log(conversationHistory);

    fileName = getDateTime();
    fileName = fileName + ".json"
    let convoHistoryToBeSerialized = JSON.stringify(conversationHistory, ["ts", "user", "text"], ' ', "\t");
// console.log(Date(608584382.000200))

    fs.writeFile(fileName, convoHistoryToBeSerialized, function (err) {
    if (err) throw err;
    console.log('File is created successfully.');
    });

    // Print results
    console.log(conversationHistory.length + " messages found in " + channelId);
  }
  catch (error) {
    console.error(error);
  }

  await say("Have successfully saved the chat to " + fileName);
});
